from django.contrib.auth.decorators import login_required
#from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from listings.forms.listing_form import CreateListingForm, ImageListingForm, \
    MakeAnOfferForm, AddHighestOfferForm, UpdateListingForm, OrderForm, CheckoutContactForm, CheckoutPaymentForm, CheckoutRateForm
from listings.models import Listing, ListingImage, CheckoutListingContact, CheckoutListingPayment, ListingUserRate
from user.models import Profile


def catalog(request):
    # if 'search_filter' in request.GET:
    #     search_filter = request.GET['search_filter']
    #     listings = [{
    #         'id': x.id,
    #         'name': x.name,
    #         'condition': x.condition.name,
    #         'price': x.price,
    #         'seller': x.seller.name,
    #         'firstImage': x.listingimage_set.first().image
    #     } for x in Listing.objects.filter(name__icontains=search_filter)]
    #     return JsonResponse({'data': listings})
    form = OrderForm(initial={'order': 'name'})
    order = 'name'
    if request.GET:
        order = request.GET['order']
        form = OrderForm(initial={'order': order})
    listings = Listing.objects.all().order_by(order).filter(is_sold=False)
    return render(request, 'listings/catalog.html', {
        'listings': listings,
        'form': form
    })


@login_required
def get_listing_by_user(request):
    profile = Profile.objects.filter(user=request.user).first()
    listings = {'listings': Listing.objects.filter(seller=profile)}
    # listings = {'listings': Listing.objects.all().order_by('name')}
    return render(request, 'listings/user_listings.html', context=listings)


@login_required
def get_listing_by_id(request, id):
    listing = get_object_or_404(Listing, pk=id)
    similar_listings = Listing.objects.all().exclude(id=listing.id).filter(
        category=listing.category)
    seller = listing.seller
    profile = Profile.objects.filter(user=request.user).first()
    return render(request, 'listings/listing_details.html', {
        'listing': listing,
        'seller': seller,
        'profile': profile,
        'similar_listings': similar_listings,
    })


@login_required
def create_listing(request):
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    form = CreateListingForm()
    image_form = ImageListingForm()
    if request.method == 'POST':
        form = CreateListingForm(data=request.POST)
        image_form = ImageListingForm(data=request.POST)
        if form.is_valid():
            listing = form.save(commit=False)
            image = image_form.save(commit=False)
            listing.seller = profile
            listing.highest_offer = 0.0
            listing.save()
            image.listing = listing
            image.save()
            return redirect('catalog')
    return render(request, 'listings/create_listing.html', {
        'form': form,
        'image_form': image_form
    })


@login_required
def delete_listing(request, id):
    listing = get_object_or_404(Listing, pk=id)
    listing.delete()
    return redirect('user_listings')


@login_required
def update_listing(request, id):
    listing = get_object_or_404(Listing, pk=id)
    image = ListingImage.objects.filter(listing=listing).first()
    if request.method == 'POST':
        form = UpdateListingForm(data=request.POST, instance=listing)
        image_form = ImageListingForm(data=request.POST, instance=image)
        if form.is_valid() and image_form.is_valid():
            form.save()
            image.save()
            return redirect('listing_details', id=id)
    return render(request, 'listings/update_listing.html', {
        'form': UpdateListingForm(instance=listing),
        'image_form': ImageListingForm(instance=image),
        'listing': listing
    })


@login_required
def make_an_offer(request, id):
    listing = get_object_or_404(Listing, pk=id)
    profile = Profile.objects.filter(user=request.user).first()
    if request.method == 'POST':
        form = MakeAnOfferForm(data=request.POST)
        listing_form = AddHighestOfferForm(data=request.POST, instance=listing)
        offer_check, msg = form.check_offer(form.data['offer'],
                                            listing.highest_offer,
                                            listing.price)
        if form.is_valid() and listing_form.is_valid() and offer_check:
            form = form.save(commit=False)
            listing_form = listing_form.save(commit=False)
            listing_form.highest_offer = form.offer
            listing_form.save()
            form.listing = listing
            form.offer_by_user = profile
            form.save()
            return redirect('listing_details', id=id)
        if offer_check == False:
            error_msg = msg
            error_highest_offer = 'Current highest offer: '\
                                  + str(listing.highest_offer)
            error_price = 'Current price: ' + str(listing.price)
            return render(request, 'listings/listing_offer.html', {
                'form': MakeAnOfferForm(instance=listing),
                'error_message': error_msg,
                'error_highest_offer': error_highest_offer,
                'error_price': error_price,
                'listing': listing,
            })
    return render(request, 'listings/listing_offer.html', {
        'form': MakeAnOfferForm(instance=listing),
        'listing': listing
    })


@login_required
def checkout_contact(request, id):
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    listing = get_object_or_404(Listing, pk=id)
    contact = CheckoutListingContact.objects.filter(listing=listing).first()
    contact_form = CheckoutContactForm(instance=contact)
    if request.method == 'POST':
        contact_form = CheckoutContactForm(data=request.POST, instance=contact)
        old_contact = CheckoutListingContact.objects.all().filter(
            listing=listing)
        if contact_form.is_valid():
            if old_contact is not None:
                delete_old(old_contact)
            contact_form = contact_form.save(commit=False)
            contact_form.listing = listing
            contact_form.user = profile
            contact_form.save()
            return redirect('checkout_payment', id=id)
    return render(request, 'listings/checkout_contact.html', {
        'listing': listing,
        'contact': contact_form,
    })

@login_required
def checkout_payment(request, id):
    user = request.user
    profile = Profile.objects.filter(user=user).first()
    listing = get_object_or_404(Listing, pk=id)
    payment = CheckoutListingPayment.objects.filter(listing=listing).first()
    payment_form = CheckoutPaymentForm(instance=payment)
    if request.method == 'POST':
        payment_form = CheckoutPaymentForm(data=request.POST, instance=payment)
        old_payment = CheckoutListingPayment.objects.all().filter(listing=listing)
        if payment_form.is_valid():
            if old_payment is not None:
                delete_old(old_payment)
            payment_form = payment_form.save(commit=False)
            payment_form.listing = listing
            payment_form.user = profile
            payment_form.save()
            return redirect('checkout_rate', id=id)
    return render(request, 'listings/checkout_payment.html', {
        'listing': listing,
        'payment': payment_form,
    })

@login_required
def checkout_rate(request, id):
    listing = get_object_or_404(Listing, pk=id)
    seller = Profile.objects.filter(user=listing.seller.user).first()
    user = seller.user
    old_rating = ListingUserRate.objects.filter(listing=listing).first()
    if old_rating is not None:
        rate_form = CheckoutRateForm(instance=old_rating)
    else:
        rate_form = CheckoutRateForm()
    if request.method == 'POST':
        rate_form = CheckoutRateForm(data=request.POST)
        if rate_form.is_valid():
            rate_form = rate_form.save(commit=False)
            old_ratings = ListingUserRate.objects.all().filter(listing=listing)
            if old_ratings is not None:
                delete_old(old_ratings)
            rate_form.user = listing.seller
            rate_form.listing = listing
            rate_form.save()
            ratings = ListingUserRate.objects.all().filter(user=seller)
            seller.avg_rating = calculate_avg_rating(ratings)
            seller.save()
            return redirect('checkout_review', id=id)
    return render(request, 'listings/checkout_rate.html', {
        'listing': listing,
        'rate': rate_form,
        'seller': seller,
    })


@login_required
def checkout_review(request, id):
    listing = get_object_or_404(Listing, pk=id)
    seller = Profile.objects.filter(user=listing.seller.user).first()
    user = seller.user
    contact = CheckoutListingContact.objects.filter(listing=listing).first()
    payment = CheckoutListingPayment.objects.filter(listing=listing).first()
    rate = ListingUserRate.objects.filter(user=seller).first()
    if request.method == 'POST':
        listing.is_sold = True
        listing.save()
        return redirect('catalog')
    return render(request, 'listings/checkout_review.html', {
        'listing': listing,
        'contact': contact,
        'payment': payment,
        'seller': seller,
        'rate': rate,
    })


def delete_old(old_data):
    for rating in old_data:
        rating.delete()


def calculate_avg_rating(ratings):
    count = 0.0
    sum_rating = 0.0
    for rating in ratings:
        count += 1
        sum_rating += rating.rating
    avg_rating = float(sum_rating / count)
    return avg_rating
