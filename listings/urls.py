from django.urls import path
from listings import views


urlpatterns = [
    path('', views.catalog, name='catalog'),
    path('<int:id>', views.get_listing_by_id, name='listing_details'),
    path('create_listing', views.create_listing, name='create_listing'),
    path('<int:id>/delete', views.delete_listing, name='delete_listing'),
    path('<int:id>/update', views.update_listing, name='update_listing'),
    path('user_listings', views.get_listing_by_user, name='user_listings'),
    path('<int:id>/offer', views.make_an_offer, name='make_an_offer'),
    path('<int:id>/checkout_contact', views.checkout_contact, name='checkout_contact'),
    path('<int:id>/checkout_payment', views.checkout_payment, name='checkout_payment'),
    path('<int:id>/checkout_rate', views.checkout_rate, name='checkout_rate'),
    path('<int:id>/checkout_review', views.checkout_review, name='checkout_review'),
]
