from django.db import models
from user.models import Profile
from django_countries.fields import CountryField


class Condition(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=255, default='N')

    def __str__(self):
        return self.name


class Listing(models.Model):
    name = models.CharField(max_length=255)
    condition = models.ForeignKey(Condition, on_delete=models.PROTECT)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    description = models.CharField(max_length=255)
    price = models.FloatField()
    highest_offer = models.FloatField(blank=True)
    seller = models.ForeignKey(Profile, on_delete=models.CASCADE)
    is_sold = models.BooleanField(blank=True, default=False)
    offer_accepted = models.BooleanField(blank=True, default=False)

    def __str__(self):
        return self.name


class ListingUserRate(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    rating = models.IntegerField()


class ListingImage(models.Model):
    image = models.CharField(max_length=9999)
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)

    def __str__(self):
        return self.image


class Offers(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    offer_by_user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    offer = models.FloatField()

    def __str__(self):
        return self.offer


class CheckoutListingContact(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=255)
    street_name = models.CharField(max_length=255)
    house_num = models.CharField(max_length=255)  # Can be 11a or 13b
    city = models.CharField(max_length=255)
    country = CountryField()
    postal_code = models.CharField(max_length=255)  # UK ahs letters and numbers


class CheckoutListingPayment(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    cardholder_name = models.CharField(max_length=255)
    card = models.CharField(max_length=19)
    expiration_year = models.IntegerField()
    expiration_month = models.IntegerField()
    cvc = models.IntegerField()

    def __str__(self):
        return self.name

