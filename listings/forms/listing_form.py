from django.forms import ModelForm, widgets, TextInput
from django import forms
from listings.models import Listing, ListingImage, Offers,\
    CheckoutListingContact, CheckoutListingPayment, ListingUserRate

from django_countries.widgets import CountrySelectWidget
from django.utils.translation import gettext_lazy as _


# CREDITCARD FORM #
class TelephoneInput(TextInput):

    # switch input type to type tel so that the numeric keyboard shows
    # on mobile devices
    input_type = 'tel'


class CreditCardField(forms.CharField):

    # validates almost all the example cards from PayPal
    # https://www.paypalobjects.com/en_US/vhelp/paypalmanager_help/
    # credit_card_numbers.htm
    cards = [
        {
            'type': 'maestro',
            'patterns': [5018, 502, 503, 506, 56, 58, 639, 6220, 67],
            'length': [12, 13, 14, 15, 16, 17, 18, 19],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'forbrugsforeningen',
            'patterns': [600],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'dankort',
            'patterns': [5019],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'visa',
            'patterns': [4],
            'length': [13, 16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'mastercard',
            'patterns': [51, 52, 53, 54, 55, 22, 23, 24, 25, 26, 27],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'amex',
            'patterns': [34, 37],
            'length': [15],
            'cvvLength': [3, 4],
            'luhn': True
        }, {
            'type': 'dinersclub',
            'patterns': [30, 36, 38, 39],
            'length': [14],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'discover',
            'patterns': [60, 64, 65, 622],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }, {
            'type': 'unionpay',
            'patterns': [62, 88],
            'length': [16, 17, 18, 19],
            'cvvLength': [3],
            'luhn': False
        }, {
            'type': 'jcb',
            'patterns': [35],
            'length': [16],
            'cvvLength': [3],
            'luhn': True
        }
    ]

    def __init__(self, placeholder=None, *args, **kwargs):
        super(CreditCardField, self).__init__(
            # override default widget
            widget=TelephoneInput(attrs={
                'placeholder': placeholder
            }), *args, **kwargs)

    default_error_messages = {
        'invalid': _(u'The credit card number is invalid'),
    }

    def card_from_number(self, num):
        # find this card, based on the card number, in the defined set of cards
        for card in self.cards:
            for pattern in card['patterns']:
                if str(pattern) == str(num)[:len(str(pattern))]:
                    return card

    # def validate_mod10(self, num):
    #     # validate card number using the Luhn (mod 10) algorithm
    #     checksum, factor = 0, 1
    #     for c in reversed(num):
    #         for c in str(factor * int(c)):
    #             checksum += int(c)
    #         factor = 3 - factor
    #     return checksum % 10 == 0

# CREDIT CARD INPUT END #


class CreateListingForm(ModelForm):
    image = forms.CharField(required=True, widget=forms.TextInput(attrs={
        'class': 'form-control'}))

    class Meta:
        model = Listing
        exclude = ['id', 'highest_offer', 'seller', 'is_sold', 'offer_accepted']
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'form-control'}),
            'condition': widgets.Select(attrs={'class': 'form-control'}),
            'category': widgets.Select(attrs={'class': 'form-control'}),
            'description': widgets.Textarea(attrs={'class': 'form-control',
                                                   'rows': 2}),
            'price': widgets.NumberInput(attrs={'class': 'form-control'}),
        }


class ImageListingForm(ModelForm):

    class Meta:
        model = ListingImage
        exclude = ['id', 'listing']
        widgets = {
            'image': widgets.TextInput(attrs={'class': 'form-control'})
        }


class UpdateListingForm(ModelForm):
    class Meta:
        model = Listing
        exclude = ['id', 'highest_offer', 'seller', 'is_sold',
                   'offer_accepted']
        widgets = {
            'name': widgets.TextInput(attrs={'class': 'form-control'}),
            'condition': widgets.Select(attrs={'class': 'form-control'}),
            'category': widgets.Select(attrs={'class': 'form-control'}),
            'description': widgets.Textarea(attrs={'class': 'form-control',
                                                   'rows': 2}),
            'price': widgets.NumberInput(attrs={'class': 'form-control'}),
        }


class AddHighestOfferForm(ModelForm):

    class Meta:
        model = Listing
        exclude = ['id', 'name', 'highest_offer', 'seller', 'condition',
                   'description', 'price', 'category', 'is_sold',
                   'offer_accepted']


class MakeAnOfferForm(ModelForm):

    class Meta:
        model = Offers
        exclude = ['id', 'listing', 'offer_by_user']
        widgets = {
            'offer': widgets.NumberInput()
        }

    @staticmethod
    def check_offer(new_offer, old_offer, price):
        msg_negative_num = 'Offer is below Zero or equal to Zero'
        msg_old_offer = 'Your offer is lower than the current highest offer'
        msg_price = 'Your offer is higher than the price'

        if float(new_offer) <= 0.0:
            return False, msg_negative_num
        if float(new_offer) < float(old_offer):
            return False, msg_old_offer
        if float(new_offer) > float(price):
            return False, msg_price

        return True, ''


class OrderForm(forms.Form):
    name = "name"
    price = "price"

    choices = (
        (name, "name"),
        (price, "price")
    )
    order = forms.ChoiceField(choices=choices)


class CheckoutContactForm(ModelForm):

    class Meta:
        model = CheckoutListingContact
        exclude = ['id', 'listing', 'user']
        widgets = {
            'full_name': widgets.TextInput(attrs={'class': 'form-control'}),
            'street_name': widgets.TextInput(attrs={'class': 'form-control'}),
            'house_num': widgets.TextInput(attrs={'class': 'form-control'}),
            'city': widgets.TextInput(attrs={'class': 'form-control'}),
            'country': CountrySelectWidget(attrs={'class': 'form-control'},
                                           layout='{widget}'),
            'postal_code': widgets.TextInput(attrs={'class': 'form-control'}),
        }


class CheckoutPaymentForm(ModelForm):
    card = CreditCardField(placeholder=u'0000 0000 0000 0000',
                           min_length=12,
                           max_length=19)

    class Meta:
        year = (
            ('22', "22"),
            ('23', "23"),
            ('24', "24"),
            ('25', "25"),
            ('26', "26"),
            ('27', "27"),
            ('28', "28"),
            ('29', "29"),
        )

        month = (
            ('01', "01"),
            ('02', "02"),
            ('03', "03"),
            ('04', "04"),
            ('05', "05"),
            ('06', "06"),
            ('07', "07"),
            ('08', "08"),
            ('09', "09"),
            ('10', "10"),
            ('11', "11"),
            ('12', "12"),
        )

        model = CheckoutListingPayment
        exclude = ['id', 'listing', 'user']
        widgets = {
            'cardholder_name': widgets.TextInput(
                attrs={'class': 'form-control'}),
            'cvc': widgets.NumberInput(
                attrs={'class': 'form-control'}),
            'expiration_year': widgets.Select(
                choices=year,
                attrs={'class': 'form-control'}),
            'expiration_month': widgets.Select(
                choices=month,
                attrs={'class': 'form-control'}),
        }


class CheckoutRateForm(ModelForm):

    class Meta:
        rate = (
            ('1', "1"),
            ('2', "2"),
            ('3', "3"),
            ('4', "4"),
            ('5', "5"),
            ('6', "6"),
            ('7', "7"),
            ('8', "8"),
            ('9', "9"),
            ('10', "10"),
        )

        model = ListingUserRate
        exclude = ['id', 'user', 'listing']
        widgets = {
            'rating': widgets.Select(choices=rate,
                                     attrs={'class': 'form-control'}),
        }
