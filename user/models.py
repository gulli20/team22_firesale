from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    bio = models.CharField(max_length=255, blank=True)
    image = models.CharField(max_length=9999, blank=True)
    avg_rating = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.name


class UserNotifications(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    message = models.CharField(max_length=255)
    status = models.BooleanField()


@receiver(post_save, sender=User)
def create_profile(sender, instance=None, created=False, **kwargs):
    if created:
        Profile.objects.create(user=instance,
                               name=instance.username,
                               avg_rating=0)
