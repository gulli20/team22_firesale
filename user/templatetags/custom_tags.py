from django import template
from user.models import Profile
from math import ceil


register = template.Library()

@register.simple_tag()
def get_profile_rating(user):
    profile = Profile.objects.filter(user=user).first()
    avg_rating = ceil(profile.avg_rating)
    return avg_rating
