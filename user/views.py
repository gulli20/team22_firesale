from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from user.models import Profile
from user.forms.user_form import ProfileForm, UserForm, UserCreateForm


def register(request):
    if request.method == 'POST':
        form = UserCreateForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    return render(request, 'user/register.html', {
        'form': UserCreateForm()
    })


@login_required
def profile(request):
    user = request.user
    profile = Profile.objects.filter(user=request.user).first()
    return render(request, 'user/profile.html', context={
        'user': user,
        'profile': profile
    })


@login_required
def edit_profile(request):
    profile = Profile.objects.filter(user=request.user).first()
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(instance=profile, data=request.POST)
        if profile_form.is_valid() and user_form.is_valid():
            profile_form = profile_form.save(commit=False)
            user_form = user_form.save(commit=False)
            user = request.user
            user_form.first_name = user.first_name
            user_form.last_name = user.last_name
            user.username = user_form.username
            user.save()
            profile_form.name = request.user.username
            profile_form.user = request.user
            profile_form.save()
            return redirect('profile')
    return render(request, 'user/edit_profile.html', {
        'form': ProfileForm(instance=profile),
        'user_form': UserForm(instance=request.user),
        'profile': profile
    })


@login_required
def user_listings(request):
    return render(request, 'user/../templates/listings/user_listings.html')
